---
title: "Mise à jour des stations de mesure"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(collectr)
library(datalibaba)
library(dplyr)
library(nngeo)
library(sf)
library(usethis)

```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)

```

# Objectif

Actualiser les **stations de mesure eau souterraine (ESO) et eau de surface (ESU)** 
à partir des tables suivantes dans une base de données PostgreSQL :

- `captages.n_captage_p_r52` (source : ARS, périmètre : région Pays de la Loire)
- `qualite_nappes_eau_souterraine.hubeau_qualite_nappes_stations` (source : Hub'eau, périmètre : bassin Loire-Bretagne)
- `sandre.n_station_mesure_eaux_surface_loire_bretagne` (source : SANDRE, périmètre : bassin Loire-Bretagne)

# Chargement des lot de données source

## Captages ARS

Chargement de la table des captages ARS : 
```{r load_captages_ars_eso, eval=FALSE}
data_ars <- datalibaba::importer_data(
  table = "n_captage_p_r52",
  schema = "captages",
  db = "si_eau",
  user = "admin")

```

## Stations de mesure des qualités des nappes d'eau souterraine (ESO)

Chargement de la table des stations ESO issues d'Hubeau :
```{r load_hubeau_qualite_nappes_stations, eval=FALSE}
data_eso_hubeau <- datalibaba::importer_data(
  table = "hubeau_qualite_nappes_stations",
  schema = "qualite_nappes_eau_souterraine",
  db = "si_eau",
  user = "admin")

```

## Stations de mesure de la qualité des eaux superficielles continentales (STQ)

Chargement de la table des stations ESU issues du SANDRE :
```{r load-hubeau_qualite_rivieres_station_pc, eval=FALSE}
data_esu_sandre <- datalibaba::importer_data(
  table = "n_station_mesure_eaux_surface_loire_bretagne",
  schema = "sandre",
  db = "si_eau",
  user = "admin")

```

# Consolidation dans des dataframes similaire 

## Stations de mesure des qualités des nappes d'eau souterraine (ESO)

Renommage des variables, ajout du code SISE-Eaux et de la source, sélection des variables :
```{r consolidate_stations_hubeau_eso, eval=FALSE}
stations_eso_hubeau <- data_eso_hubeau |> 
  dplyr::rename(libelle_station = nom_commune,
                date_creation = date_debut_mesure,
                code_commune = code_insee) |>
  dplyr::mutate(code_sise_eaux = NA,
                source = "HUBEAU",
                nature_eau = "ESO",
                code_masse_eau = NA) |>
  dplyr::select(code_bss = bss_id,
                code_sise_eaux,
                libelle_station,
                nature_eau,
                date_creation,
                source,
                code_masse_eau = codes_masse_eau_edl,
                code_commune,
                the_geom)

# Convertir les dates de la variable date_creation
stations_eso_hubeau$date_creation <- as.Date(stations_eso_hubeau$date_creation)

```

Remplacer les valeurs vides dans `code_sise_eaux` par NA dans `stations_eso_hubeau` :
```{r na_code_sise_eaux, eval=FALSE}
stations_eso_hubeau <- stations_eso_hubeau |>
  dplyr::mutate(code_sise_eaux = dplyr::if_else(code_sise_eaux == "", NA_character_, code_sise_eaux))

```

Réaliser la jointure sur la variable `code_bss` :
```{r update_code_sise_eaux, eval=FALSE}
# Supprimer l'objet géométrie du dataframe data_ars
data_ars_no_geom <- data_ars |> sf::st_drop_geometry()

# Réaliser une jointure attributaire
stations_eso_hubeau <- stations_eso_hubeau |>
  dplyr::left_join(data_ars_no_geom, by = c("code_bss" = "code_bss")) |>
  dplyr::mutate(code_sise_eaux = ifelse(is.na(code_sise_eaux), code_captage, code_sise_eaux)) |>
  dplyr::select(-code_captage) # Supprime la colonne code_captage
```

Identification des doublons générés par la jointure de récupération des `code_sise_eaux` :
```{r get_duplicates_code_sise_eaux, eval=FALSE}
# Compter les occurrences de chaque code_station
doublons_stations_code_sise_eaux <- stations_eso_hubeau |>
  dplyr::group_by(code_bss) |>
  dplyr::tally(name = "n") |>
  dplyr::filter(n > 1)

# Supprimer l'objet géométrique pour pouvoir exécuter la jointure attributaire
  doublons_stations_code_sise_eaux <- doublons_stations_code_sise_eaux |>
  sf::st_drop_geometry()
  stations_eso_hubeau_no_geom <- stations_eso_hubeau |> sf::st_drop_geometry()

# Joindre les informations additionnelles
doublons_stations_code_sise_eaux <- doublons_stations_code_sise_eaux |>
  dplyr::left_join(stations_eso_hubeau_no_geom, by = "code_bss") |>
  dplyr::select(code_bss, n, code_sise_eaux, libelle_station)

# Visualiser les doublons
print(doublons_stations_code_sise_eaux)

```

Suppression des doublons :
```{r delete_duplicates_code_sise_eaux, eval=FALSE}
stations_eso_hubeau <- stations_eso_hubeau |>
  dplyr::group_by(code_bss, the_geom) |> dplyr::slice(1) |> dplyr::ungroup()

```

## Stations de mesure de la qualité des eaux superficielles continentales (STQ)

Sélection des variables et ajout de la source :
```{r consolidate_stations_esu_sandre, eval=FALSE}
stations_esu_sandre <- data_esu_sandre |> 
  dplyr::mutate(code_bss = NA,
                source = "SANDRE",
                nature_eau = "ESU") |>
  dplyr::select(code_bss,
                code_sise_eaux = code_station,
                libelle_station,
                date_creation,
                nature_eau,
                source,
                code_masse_eau,
                code_commune,
                the_geom)

# Convertir les dates de la variable date_creation
stations_esu_sandre$date_creation <- as.Date(stations_esu_sandre$date_creation)

```

## Captages ARS

Suppression des captages ARS avec des géométries vides :
```{r delete-empty-geom, eval=FALSE}
data_ars_with_geom = data_ars[!sf::st_is_empty(data_ars),,drop=FALSE] 

```

## Sélection des captages ARS différents des stations ESO de Hub'eau et des stations ESU du SANDRE

```{r r select_captages_ars_not_hubeau_sandre, eval=FALSE}
# Fusion des opérations pour sélectionner les captages ARS différents des stations ESO Hub'eau et ESU SANDRE
stations_ars_not_hubeau_sandre <- data_ars_with_geom |> 
  # Supprimer la géométrie pour effectuer des jointures
  sf::st_drop_geometry() |> 
  # Exclure les stations présentes dans stations_eso_hubeau et stations_esu_sandre
  dplyr::anti_join(stations_eso_hubeau, by = c("code_bss" = "code_bss")) |> 
  dplyr::anti_join(stations_esu_sandre, by = c("code_captage" = "code_sise_eaux")) |> 
  # Récupérer les géométries par jointure avec data_ars
  dplyr::left_join(data_ars, by = "code_captage") |> 
  # Désélectionner toutes les variables finissant par .y
  dplyr::select(-ends_with(".y"))

# Renommer les colonnes pour supprimer le suffixe .x
names(stations_ars_not_hubeau_sandre) <- gsub("\\.x$", "", names(stations_ars_not_hubeau_sandre))

```

## Consolidation des stations ARS

Ajout de la source et sélection des variables :
```{r consolidate_stations_ars, eval=FALSE}
# Ajouter la variable `source` et sélectionner les variables nécessaires
stations_ars <- stations_ars_not_hubeau_sandre |>
  dplyr::mutate(source = "ARS") |>
  dplyr::select(code_bss = code_bss,
                code_sise_eaux = code_captage,
                libelle_station = nom_captage,
                nature_eau = nature_eau_captage,
                date_creation = date_etat_installation,
                source,
                code_commune = code_commune_captage,
                the_geom)

```

## Ajout du `code_masse_eau` pour les stations ARS

Chargement de la table des bassins versants France entière :
```{r load_bassin_versant, eval=FALSE}
n_bassin_versant_specifique_000 <- datalibaba::importer_data(
  table = "n_bassin_versant_specifique_000",
  schema = "sandre",
  db = "si_eau",
  user = "admin"
)

```

Requête spatiale pour la jointure de la variable `code_masse_eau`
```{r update-codes-bassin-versant-masse-eau, eval=FALSE}
# Convertir le dataframe `stations_ars` en objet sf
stations_ars <- sf::st_as_sf(stations_ars, sf_column_name = "the_geom")

# Réaliser une jointure spatiale pour le dataframe `stations_ars`
stations_ars <- stations_ars |>
  sf::st_join(n_bassin_versant_specifique_000 |> 
                dplyr::select(code_masse_eau), join = sf::st_within, left = TRUE)

```

## Fusion des trois dataframes

```{r merge_stations, eval=FALSE}
station_full <- dplyr::bind_rows(stations_eso_hubeau, stations_esu_sandre, stations_ars)

```

# Intersection des stations ESU avec les SAGE de la région et les limites régionales

Chargement de la table des SAGE en Pays de la Loire :
```{r load_n_sage_r52, eval=FALSE}
n_sage_r52 <- datalibaba::importer_data(
  table = "n_sage_r52",
  schema = "zonages_de_gestion",
  db = "si_eau"
)
```

Chargement de la table des limites de la région :
```{r load_n_region_exp_r52, eval=FALSE}
n_region_exp_r52 <- datalibaba::importer_data(
  table = "n_region_exp_r52",
  schema = "adminexpress",
  db = "referentiels"
)
```

Vérification des objets sf :
```{r st_as_sf, eval=FALSE}
# Vérifier que les deux dataframes sont en format 'sf'
station_full <- sf::st_as_sf(station_full)
n_sage_r52 <- sf::st_as_sf(n_sage_r52)
n_region_exp_r52 <- sf::st_as_sf(n_region_exp_r52)

```

Création d'un polygone fusionnant SAGE et région :
```{r limit_sage, eval=FALSE}
# Identification des noms de colonnes communs aux deux objets
colonnes_communes <- intersect(names(n_region_exp_r52), names(n_sage_r52))

# Sélection des colonnes communes dans chaque objet
n_region_exp_r52 <- n_region_exp_r52[, colonnes_communes]
n_sage_r52 <- n_sage_r52[, colonnes_communes]

# Combinaison des deux objets dans un seul objet sf
# (création d'une seule collection de géométries pour pouvoir les fusionner)
objets_combines <- rbind(n_region_exp_r52, n_sage_r52)

# Agrégation de tous les polygones en un seul avec st_union()
# (si les polygones se chevauchent ou sont adjacents, ils seront fusionnés)
n_sage_r52_union <- sf::st_union(objets_combines)

# Correction de la géométrie, pour s'assurer qu'elle est valide
n_sage_r52_valid <- sf::st_make_valid(n_sage_r52_union)

# Suppresion des trous internes en récupérant uniquement la limite extérieure
n_sage_r52_limit <- nngeo::st_remove_holes(n_sage_r52_valid)

```

Sélection des stations ESU présentes dans les SAGE et la région :
```{r st_intersects_stations_sage, eval=FALSE}
# Filtrer les stations ESU présentes dans les SAGE de la région
station_sage_r52 <- sf::st_filter(station_full, n_sage_r52_limit)

```

# Ajout des variables manquantes nécessaires

## Ajout de la variable `id_station`

```{r add_id_station, eval=FALSE}
# Ajouter la variable `id_station`
station_sage_r52 <- station_sage_r52 |>
  # Trier par code_commune
  dplyr::arrange(code_commune) |> 
  # Ajouter un ID incrémental
  dplyr::mutate(id_station = dplyr::row_number()) 

```


## Ajout de la variable `code_naiades`
Cet ajout ne concerne que les stations ESU :
```{r add_code_naiades, eval=FALSE}
# Créer et alimenter la variable `code_naiades` à partir de `code_sise_eaux`
station_sage_r52 <- station_sage_r52 |> 
  dplyr::mutate(
    code_naiades = dplyr::case_when(
      startsWith(code_sise_eaux, "044") ~ sub("^044", "BS", code_sise_eaux),
      startsWith(code_sise_eaux, "049") ~ sub("^049", "BX", code_sise_eaux),
      startsWith(code_sise_eaux, "053") ~ sub("^053", "CB", code_sise_eaux),
      NA ~ code_sise_eaux  # Remplacer les autres codes par NA
    ))

```

## Ajout de la variable `code_bassin_versant`

Jointure spatiale entre `n_bassin_versant_specifique_000` et `station_sage_r52` :
```{r add_codes_bassin_versant, eval=FALSE}
# Réaliser une jointure spatiale pour récupérer la variable `code_bassin_versant`
station_sage_r52 <- station_sage_r52 |>
  sf::st_join(n_bassin_versant_specifique_000 |> 
                dplyr::select(code_bassin_versant = code_bassin_versant_specifique), 
              join = sf::st_within, left = TRUE)

```

Identification des doublons générés par la superposition de périmètres dans la table des bassins versants :
```{r get_duplicates_code_bassin_versant, eval=FALSE}
# Compter les occurrences de chaque id_station
doublons_code_bassin_versant <- station_sage_r52 |>
  dplyr::group_by(id_station) |>
  dplyr::tally(name = "n") |>
  dplyr::filter(n > 1)

# Supprimer l'objet géométrique pour pouvoir exécuter la jointure attributaire
doublons_code_bassin_versant <- doublons_code_bassin_versant |>
  sf::st_drop_geometry()

# Joindre les informations additionnelles
doublons_code_bassin_versant <- doublons_code_bassin_versant |>
  dplyr::left_join(station_sage_r52, by = "id_station") |>
  dplyr::left_join(n_bassin_versant_specifique_000, by = c("code_bassin_versant" = "code_bassin_versant_specifique")) |>
  dplyr::select(id_station, n, libelle_station, code_bassin_versant, nom_bassin_versant_specifique, the_geom.x)

# Visualiser les doublons
print(doublons_code_bassin_versant)

```

Suppression des doublons (optionnel) :
```{r delete_duplicates_code_bassin_versant, eval=FALSE}
station_sage_r52 <- station_sage_r52 |>
  dplyr::group_by(id_station, the_geom) |> dplyr::slice(1) |> dplyr::ungroup()

```

## Ajout de la variable `code_sage`

Chargement de la table des SAGE en Pays de la Loire :
```{r load-sage, eval=FALSE}
n_sage_r52 <- datalibaba::importer_data(
  table = "n_sage_r52",
  schema = "zonages_de_gestion",
  db = "si_eau",
  user = "admin"
)

```

Requête spatiale pour la jointure du `code_sage` dans `station_sage_r52` :
```{r add_code_sage, eval=FALSE}
# Réaliser une jointure spatiale
station_sage_r52 <- station_sage_r52 |>
  sf::st_join(n_sage_r52 |> dplyr::select(code_sage = code), join = sf::st_within, left = TRUE)
```

Identification des doublons générés par la superposition de périmètres dans la table des SAGE :
```{r get_station_duplicates_code_sage, eval=FALSE}
# Compter les occurrences de chaque code_station
doublons_stations_code_sage <- station_sage_r52 |>
  dplyr::group_by(id_station) |>
  dplyr::tally(name = "n") |>
  dplyr::filter(n > 1)

# Supprimer l'objet géométrique pour pouvoir exécuter la jointure attributaire
doublons_stations_code_sage <- doublons_stations_code_sage |>
  sf::st_drop_geometry()

# Joindre les informations additionnelles
doublons_stations_code_sage <- doublons_stations_code_sage |>
  dplyr::left_join(station_sage_r52, by = "id_station") |>
  dplyr::left_join(n_sage_r52, by = c("code_sage" = "code")) |>
  dplyr::select(id_station, n, libelle_station, code_sage, nom, the_geom.x)

# Visualiser les doublons
print(doublons_stations_code_sage)
```

Suppression des doublons (optionnel) :
```{r delete_station_duplicates_code_sage, eval=FALSE}
station_sage_r52 <- station_sage_r52 |>
  dplyr::group_by(id_station, the_geom) |> dplyr::slice(1) |> dplyr::ungroup()
```

Ajout de la valeur `SAGE00000` si la valeur de `code_sage` est NA :
```{r replace-code-sage-na, eval=FALSE}
station_sage_r52 <- station_sage_r52 |> 
  dplyr::mutate(code_sage = tidyr::replace_na(code_sage, "SAGE00000"))
```

## Ajout de la variable `captage_prioritaire`

Chargement de la table des captages prioritaires de la région :
```{r load_r_captage_prioritaire_r52, eval=FALSE}
r_captage_prioritaire_r52 <- datalibaba::importer_data(
  table = "r_captage_prioritaire_r52",
  schema = "captages",
  db = "si_eau",
  user = "admin"
)

```

Ajout de la variable captage_prioritaire
```{r update-captage-prioriaire, eval=FALSE}
# Alimenter la variable en vérifiant la présence du `code_si_eaux` dans `r_captage_prioritaire_r52`
station_sage_r52 <- station_sage_r52 |>
  dplyr::mutate(captage_prioritaire = 
                  code_sise_eaux %in% r_captage_prioritaire_r52$code_sise_eaux)
```

# Publication en base

## Sélection des variables à conserver dans le dataframe final

```{r order_variables, eval=FALSE}
# Modifier l'ordre des variables pour publication dans une table en base
r_station_mesure_p_r52 <- station_sage_r52 |> 
  dplyr::select(
    id_station,
    code_bss,
    code_sise_eaux,
    code_naiades,
    libelle_station, 
    nature_eau,
    date_creation, 
    source,
    code_masse_eau,
    code_bassin_versant,
    code_commune,
    code_sage,
    captage_prioritaire,
    the_geom
  )
```


## Création dynamique du nom de la table avec l'année en cours

```{r create_table_name, eval=FALSE}
# Définir l'année en cours
current_year <- format(Sys.Date(), "%Y")

# Construire dynamiquement le nom de la table
table_name <- paste0("r_station_mesure_p_", current_year, "_r52")
```


## Archivage de la version précédente de la table

La version précédente de l'export est stockée dans un schéma d'archive :
```{r archive_old_table, eval=FALSE}
collectr::archive_table(database = "si_eau",
                        table = table_name, 
                        schema = "stations",
                        new_schema = "zz_archives",
                        role = "admin")
```

## Publication de la table actualisée

La table actualisée écrase la précédente version :
```{r publish_new_table, eval=FALSE}
# Publier la table en base
datalibaba::poster_data(data = r_station_mesure_p_r52,
                        table = table_name,
                        schema = "stations",
                        db = "si_eau",
                        pk = "id_station",
                        overwrite = TRUE,
                        user = "admin")

```

## Mise à jour de la géométrie de la table `r_station_mesure_p_current_year_r52`

```{r update_geometry_r_station_mesure_p_current_year_r52, eval=FALSE}
# Connexion à la base de données PostgreSQL
connexion <- datalibaba::connect_to_db(db = "si_eau", user = "admin")

# Requête SQL pour ajouter les contraintes à la colonne the_geom
# Construire la requête SQL en remplaçant dynamiquement le nom de la table
constraint_query <- sprintf(
  "ALTER TABLE stations.%s
   ADD CONSTRAINT enforce_geotype_the_geom 
   CHECK (geometrytype(the_geom) = 'MULTIPOINT'::text 
          OR geometrytype(the_geom) = 'POINT'::text 
          OR the_geom IS NULL);",
  table_name
)

# Exécution de la requête ALTER TABLE
DBI::dbExecute(connexion, constraint_query)

# Fermeture de la connexion
DBI::dbDisconnect(connexion)

```

## Ajout des commentaires de la table actualisée

### Description de la table actualisée

```{r publish_new_table_comment, eval=FALSE}
date_today  <- format(Sys.time(), format = "%d/%m/%Y")
datalibaba::commenter_table(
  comment = glue::glue("Stations de mesure millésime ", 
                       current_year, 
                       " (source : ARS + Hub'eau + SANDRE, ", 
                       date_today, ")"),
  table = table_name,
  schema = "stations",
  db = "si_eau",
  user = "admin"
)

```

### Description des champs de la table actualisée

```{r publish_new_fields_comments, eval=FALSE}
# Création d'une liste avec les noms des variables et leurs descriptions
variables <- c(
  "id_station", "Identifiant unique de la station de mesur",
  "code_bss", "Code BSS de la station de mesure",
  "code_sise_eaux", "Code SISE-EAUX de la station de mesure",  
  "code_naiades", "Code Naïades de la station de mesure",
  "libelle_station", "Nom de la station de mesure",
  "nature_eau", "Nature de l'eau de la station de mesure",
  "date_creation", "Date de création de la station de mesure",
  "source", "Source de la donnée",
  "code_masse_eau", "Code de la masse d'eau de la station de mesure",
  "code_bassin_versant", "Code du bassin versant de la station de mesure",
  "code_commune", "Code INSEE  de la commune de la station de mesure",
  "code_sage", "Code du SAGE de la station de mesure",
  "captage_prioritaire", "Captaire prioritaire (T/F)",
  "the_geom", "Géométrie ponctuelle de la station de mesure"
)

# Création du dataframe avec les variables
comments <- data.frame(
  nom_variable = variables[seq(1, length(variables), by = 2)],
  libelle_variable = variables[seq(2, length(variables), by = 2)],
  stringsAsFactors = FALSE
)

# Publication du dictionnaire de données
datalibaba::post_dico_attr(dico = comments,
                           table = table_name,
                           schema = "stations",
                           db = "si_eau",
                           user = "admin")

```

## Mise à jour du skeleton

```{r development-create-template, eval=FALSE}
# Créer de l'arborescence et des fichiers du template
usethis::use_rmarkdown_template(
  template_name = "Mise à jour des stations de mesure ESO et ESU",
  template_dir = "update_r_station_mesure_p_r52",
  template_description = "Mise à jour des stations de mesure ESO et ES",
  template_create_dir = TRUE
)

# Définir les chemins source et destination
source_file <- "dev/flat_update_r_station_mesure_p_r52.Rmd"
destination_dir <- "inst/rmarkdown/templates/update_r_station_mesure_p_r52/skeleton"
destination_file <- file.path(destination_dir, "skeleton.Rmd")

# Copier et renommer le fichier
file.copy(from = source_file, to = destination_file, overwrite = TRUE)
message("File copied and renamed successfully.")
```

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_update_r_station_mesure_p_r52.Rmd", vignette_name = "Mise à jour des stations de mesure")
```

