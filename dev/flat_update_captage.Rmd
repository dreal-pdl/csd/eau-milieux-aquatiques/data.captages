---
title: "Mise à jour des captages ARS"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development}
library(readxl)
library(lubridate)
library(dplyr)
library(utils)
library(collectr)
library(sf)
library(datalibaba)
library(glue)
library(usethis)

```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)

```

# Objectif

Actualiser **la table des captages de la région** (`captages.n_captage_p_r52`) 
à partir d'un tableur au format Excel transmis annuellement par l'ARS.

# Chargement des lots de données

## Captages ARS (SISE-EAUX)

### Chargement des captages du dernier livrable en date
L'export transmis par l'ARS est importé ainsi que la date du fichier (qui sera utilisée ultérieurement comme métadonnée) :
```{r load_data, eval=FALSE}
# Make the dataset file available to the current Rmd during development
pkgload::load_all(path = here::here(), export_all = FALSE)

# Spécifier le chemin vers le fichier Excel
file_path <- "T:/datalab/SRNP_DEMA_SI_EAU/CAPTAGES_ZONAGES_ASSOCIES/DONNEES_CLIENT/CAPTAGES/CAP_SISE_13_02_2024.xlsx"

# Stocker la date de dernier enregistrement du fichier
last_modified_date <- format(file.info(file_path)$ctime,"%d/%m/%Y")

# Lire le fichier Excel dans un dataframe
data <- readxl::read_excel(file_path)

# Vérifier le type de données dans la colonne "INS - Début d'usage - Date"
str(data)

# Convertir la colonne "INS - Début d'usage - Date" en numérique
data <- data |>
  dplyr::mutate(`INS - Début d'usage - Date` = as.numeric(`INS - Début d'usage - Date`))

# Convertir les valeurs numériques en dates
data <- data |>
  dplyr::mutate(`INS - Début d'usage - Date` = as.Date(`INS - Début d'usage - Date`, origin = "1899-12-30"))

```

### Renommage des variables

Les variables de l'export sont renommées pour correspondre aux champs de la 
table en base :
```{r consolidate_data, eval=FALSE}
captage <- data |>
  dplyr::rename(departement_captage = "INS - Dépt gest - Code",
         code_unite_gestion = "UGE - Code national",
         nom_unite_gestion = "UGE - Nom",
         code_captage = "INS - Code national",
         nom_captage = "INS - Nom",
         type_installation = "INS - Type - Code",
         nature_eau_captage = "INS - Nature de l'eau - Code",
         usage_captage = "INS - Usage direct - Code",
         etat_installation = "INS - Etat - Code",
         date_etat_installation = "INS - Début d'usage - Date",
         code_responsable_suivi = "INS - Resp. suivi - Code",
         motif_abandon = "INS - Motif d'abandon - Libellé",
         code_commune_captage = "COM - Code INSEE (5 car)",
         nom_commune_captage = "COM - Nom",
         code_bss = "CAP - BSS - Code",
         designation_bss = "CAP - BSS - Désignation",
         coordonnee_x = "PSV - Coordonnée X",
         coordonnee_y = "PSV - Coordonnée Y",
         coordonnee_z = "PSV - Coordonnée Z",
         debit_moyen_m3j = "INS - Débit moyen/jour - m3/j")

```

## Captages ARS (SISE-EAUX) gérés par un autre département
:warning: On récupère de façon temporaire ces captages (principalement gérés 
par le 35) dans un livrable plus ancien car ils ne sont pas disponibles dans
la livraison la plus récente. il faut donc vérifier le contenu du livrable 
avant d'exécuter le chunk suivant, après chaque livraison. 

### Chargement des captages manquants
```{r load_missing_data, eval=FALSE}
# Make the dataset file available to the current Rmd during development
pkgload::load_all(path = here::here(), export_all = FALSE)

# Spécifier le chemin vers le fichier Excel
file_path <- "T:/datalab/SRNP_DEMA_SI_EAU/CAPTAGES_ZONAGES_ASSOCIES/DONNEES_CLIENT/CAPTAGES/@archives/CAP_SISE_11_11_2020.xlsx"

# Lire le fichier Excel dans un dataframe et filtrer pour ne conserver que les enregistrements du 35
missing_data <- readxl::read_excel(file_path, skip = 5) |> 
  dplyr::filter(`Dpt Gest.` == "035")

```

### Renommage et sélection des variables
```{r load_missing_data, eval=FALSE}
# Adapter le format du dataframe pour le fusionner avec les données plus récentes
captage_035 <- missing_data |>
  dplyr::rename(departement_captage = "Dpt Gest.",
         code_unite_gestion = "UGE - Code",
         nom_unite_gestion = "UGE - Nom",
         code_captage = "Code CAP",
         nom_captage = "Nom Captage",
         type_installation = "Type ins",
         nature_eau_captage = "NatEau",
         usage_captage = "Usage",
         etat_installation = "Etat",
         date_etat_installation = "DébUsage",
         motif_abandon = "Motif abandon",
         code_commune_captage = "Com Code",
         nom_commune_captage = "Com Nom",
         code_bss = "BSS code",
         designation_bss = "Dés. BSS",
         coordonnee_x = "X - Lbt2e",
         coordonnee_y = "Y - Lbt2e",
         coordonnee_z = "Z",
         debit_moyen_m3j = "moy m3/j") |>
  dplyr::mutate(date_etat_installation = as.Date(date_etat_installation),
                code_responsable_suivi = NA,
                code_commune_captage = as.character(code_commune_captage)
                ) |>
  dplyr::select(
    departement_captage,
    code_unite_gestion,
    nom_unite_gestion,
    code_captage,
    nom_captage,
    type_installation,
    nature_eau_captage,
    usage_captage,
    etat_installation,
    date_etat_installation,
    code_responsable_suivi,
    motif_abandon,
    code_commune_captage,
    nom_commune_captage,
    code_bss,
    designation_bss,
    coordonnee_x,
    coordonnee_y,
    coordonnee_z,
    debit_moyen_m3j
  )


```

## Fusion des deux dataframes

On rassemble les données des dataframes `captage` `captage_035` pour disposer
du référentiel le plus complet possible :
```{r merge_captages, eval=FALSE}
# Fusionner les deux dataframes
captage_full <- dplyr::bind_rows(captage, captage_035) |>
  dplyr::arrange(code_captage)

```

# Consolidation du dataframe

## Mise à jour du code BSS

Chargement de la table de passage des codes BSS anciens à nouveaux :
```{r load_table_passage_bss, eval=FALSE}
# Charger la table `captages.table_de_passage_bss_000`
table_de_passage_bss_000 <- datalibaba::importer_data(
  table = "table_de_passage_bss_000",
  schema = "captages",
  db = "si_eau",
  user = "admin"
)

```

Les anciens codes BSS de l'export transmis par l'ARS sont remplacés par les 
nouveaux codes issus de la table de passage fournie par le BRGM lorsque la 
jointure est possible :
```{r update_code_bss, eval=FALSE}
# Mettre à jour la variable `code_bss` du dataframe `captage`
captage_bss <- captage_full |>
  dplyr::left_join(table_de_passage_bss_000, by = c("code_bss" = "indice")) |>
  dplyr::mutate(code_bss = ifelse(!is.na(nouvel_identifiant), nouvel_identifiant, code_bss)) |>
  dplyr::select(departement_captage:debit_moyen_m3j)

```

Vérification des résultats de la jointure :
```{r dev-count_empty_bss, eval=FALSE}
# Compter le nombre de valeurs vides dans captage_full
nb_code_bss_vide_captage <- captage_full |>
  dplyr::summarise(nb_code_bss_vide = sum(is.na(code_bss)))

# Compter le nombre de valeurs vides dans captage_bss
nb_code_bss_vide_captage_bss <- captage_bss |>
  dplyr::summarise(nb_code_bss_vide = sum(is.na(code_bss)))

# Afficher les résultats
print(nb_code_bss_vide_captage)
print(nb_code_bss_vide_captage_bss)
```

Sélection des captages ESO sans codes BSS malgré la jointure :
```{r select_captage_eso_sans_code_bss, eval=FALSE}
captage_eso_sans_code_bss <- captage_bss |>
  dplyr::filter(!grepl("^BSS", code_bss) & nature_eau_captage == "ESO")
```

```{r save_captage_eso_sans_code_bss, eval=FALSE}
# Enregistrer le dataframe captage_sans_geom dans un fichier CSV
utils::write.csv(captage_eso_sans_code_bss, 
                 file = "data/captage_eso_sans_code_bss.csv", 
                 row.names = FALSE)

# Afficher un message de confirmation
cat("Le fichier captage_eso_sans_code_bss.csv a été enregistré avec succès.\n")
```

## Vérification de la correspondance des variables avec les champs stockés en base

À ce stade, la vérification doit faire ressortir l'absence de géométrie (champs `the_geom`) :
```{r check_attributes_n_captage_p_r52, eval=FALSE}
# Comparer la structure du dataframe avec celle de la table en base 
collectr::check_structure_table(dataframe = captage, 
                                database = "si_eau",
                                table = "n_captage_p_r52", 
                                schema = "captages",
                                role = "admin")

```

## Création d'un dataframe avec les enregistrements sans coordonnées ou des coordonées erronées

Filtrage des enregistrements sans coordonnées valides (avec une valeur NA, 1 ou 3) :
```{r filter_captage_sans_geom, eval=FALSE}
# Créer un nouveau dataframe avec les captages sans coordonnées valides
captage_sans_geom <- captage_bss |>
  dplyr::mutate(coordonnee_x = ifelse(coordonnee_x %in% c(1, 3), NA, coordonnee_x),
                coordonnee_y = ifelse(coordonnee_y %in% c(1, 3), NA, coordonnee_y)) |>
  dplyr::filter(is.na(coordonnee_x) | is.na(coordonnee_y))

```

```{r save_captage_sans_geom, eval=FALSE}
# Enregistrer le dataframe captage_sans_geom dans un fichier CSV
utils::write.csv(captage_sans_geom, 
                 file = "data/captage_sans_geom.csv", 
                 row.names = FALSE)

# Afficher un message de confirmation
cat("Le fichier captage_sans_geom.csv a été enregistré avec succès.\n")

```

## Encodage de la géométrie

Suppression des enregistrements sans coordonnées valides avant encodage :
```{r create_geom, eval=FALSE}
# Supprimer les lignes sans coordonnées du dataframe d'origine
captage_with_xy <- captage_bss |>
  dplyr::mutate(coordonnee_x = ifelse(coordonnee_x %in% c(1, 3), NA, coordonnee_x),
                coordonnee_y = ifelse(coordonnee_y %in% c(1, 3), NA, coordonnee_y)) |>
  dplyr::filter(!is.na(coordonnee_x) & !is.na(coordonnee_y))

# Créer une géométrie ponctuelle à partir de coordonnee_x et coordonnee_y
captage_geom <- sf::st_as_sf(captage_with_xy, 
                        coords = c("coordonnee_x", "coordonnee_y"), 
                        crs = 2154,
                        remove = FALSE) |>
  dplyr::rename(the_geom = geometry)

```

## Fusion des deux dataframes avec et sans géométrie

Reconstitution du lot de données complet (avec et sans géométrie valide) :
```{r merge_dataframes, eval=FALSE}
n_captage_p_r52 <- dplyr::bind_rows(captage_geom, captage_sans_geom)

```

# Publication de la table en base

## Archivage de la version précédente de la table

La version précédente de l'export est stockée dans un schéma d'archive :
```{r archive_old_table, eval=FALSE}
collectr::archive_table(database = "si_eau",
                        table = "n_captage_p_r52", 
                        schema = "captages",
                        new_schema = "zz_archives",
                        role = "admin")

```

## Ajout de la contrainté géomatique concernant le type de géométries

```{r dev-update_geometry_n_captage_p_r52}
# Connexion à la base de données PostgreSQL
connexion <- datalibaba::connect_to_db(db = "si_eau", user = "admin")

# Requête SQL pour ajouter la contrainte à la colonne the_geom
constraint_query <-
"ALTER TABLE captages.n_captage_p_r52
ADD CONSTRAINT enforce_geotype_the_geom CHECK (geometrytype(the_geom) = 'MULTIPOINT'::text OR geometrytype(the_geom) = 'POINT'::text OR the_geom IS NULL);"

# Exécution de la requête ALTER TABLE
DBI::dbExecute(connexion, constraint_query)

# Fermeture de la connexion
DBI::dbDisconnect(connexion)

```

## Récupération des commentaires de la version précédente de la table 

```{r get_comments_old_table, eval=FALSE}
n_captage_p_r52_comments <- datalibaba::get_table_comments(table = "n_captage_p_r52",
                                                           schema = "captages",
                                                           db = "si_eau",
                                                           user = "admin") |> 
  dplyr::filter(!is.na(nom_col)) |> 
  dplyr::select(nom_col, commentaire) |>
  dplyr::arrange(nom_col)

```

## Publication de la table actualisée

La table actualisée écrase la précédente version :
```{r publish_new_table, eval=FALSE}
datalibaba::poster_data(data = n_captage_p_r52,
                        table = "n_captage_p_r52",
                        schema = "captages",
                        db = "si_eau",
                        pk = "code_captage",
                        overwrite = TRUE,
                        user = "admin")

```

## Publication de la description de la table actualisée

```{r publish_new_table_comment, eval=FALSE}
datalibaba::commenter_table(
  comment = glue::glue("Captages de la région Pays de la Loire (source : ARS, ", last_modified_date, ")"),
  table = "n_captage_p_r52",
  schema = "captages",
  db = "si_eau",
  user = "admin")

```

## Publication des commentaires des champs de la table actualisée

```{r publish_new_fields_comments, eval=FALSE}
datalibaba::post_dico_attr(dico = n_captage_p_r52_comments,
                           table = "n_captage_p_r52",
                           schema = "captages",
                           db = "si_eau",
                           user = "admin")

```

```{r development-create-template, eval=FALSE}
# Créer de l'arborescence et des fichiers du template
usethis::use_rmarkdown_template(
  template_name = "Mise à jour des captages ARS",
  template_dir = "update_captage",
  template_description = "Mise à jour des captages ARS",
  template_create_dir = TRUE
)

# Définir les chemins source et destination
source_file <- "dev/flat_update_captage.Rmd"
destination_dir <- "inst/rmarkdown/templates/update_captage/skeleton"
destination_file <- file.path(destination_dir, "skeleton.Rmd")

# Copier et renommer le fichier
file.copy(from = source_file, to = destination_file, overwrite = TRUE)
message("File copied and renamed successfully.")
```

```{r development-inflate, eval=FALSE}
# Keep eval=FALSE to avoid infinite loop in case you hit the knit button
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_update_captage.Rmd", vignette_name = "Mise à jour des captages ARS")
```
