
<!-- README.md is generated from README.Rmd. Please edit that file -->

# data.captages <img src='man/figures/logo.png' align="right" height="139" />

<!-- badges: start -->

[![Latest
Release](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/data.captages/-/badges/release.svg)](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/data.captages/-/releases)
[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Lifecycle:
stable](https://img.shields.io/badge/lifecycle-stable-green.svg)](https://lifecycle.r-lib.org/articles/stages.html#stable)
[![pipeline
status](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/data.captages/badges/main/pipeline.svg)](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/data.captages/-/commits/main)

<!-- badges: end -->

L’objectif du package {data.captages} est de faciliter l’actualisation
des lots de données suivants dans une base de données PostgreSQL locale
:

- **Captages** : source ARS
- **Captages en eau potable** : source ARS
- **Stations de mesure des qualités des nappes d’eau souterraine** :
  source [API Hub’eau Qualité des nappes d’eau
  souterraine](https://hubeau.eaufrance.fr/page/api-qualite-nappes)
- **Stations de mesures physicochimique sur des cours d’eau et plan
  d’eau** : source [API Hub’eau Qualité des cours
  d’eau](https://hubeau.eaufrance.fr/page/api-qualite-cours-deau)
- **Stations de mesure eau souterraine (ESO)** : source ARS + Hub’eau
- **Stations de mesures eau de surface (ESU)** : source ARS + SANDRE

La documentation du package est consultable sur ce site :
<https://dreal-pdl.gitlab-pages.din.developpement-durable.gouv.fr/csd/eau-milieux-aquatiques/data.captages/>

## Installation

Vous pouvez installer le package à partir du dépôt GitLab de cette façon
:

``` r
remotes::install_gitlab('dreal-pdl/csd/eau-milieux-aquatiques/data.captages', host="gitlab-forge.din.developpement-durable.gouv.fr")
```

## Utilisation

### Chargement du package

``` r
library(data.captages)
```

### Chargement d’un template

Ouvrir un nouveau fichier RMarkdown via *New File \> Rmarkdown* :

<figure>
<img
src="https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/eau-milieux-aquatiques/data.captages/-/raw/main/inst/images/new_file_rmarkdown.png"
alt="Capture d’écran du menu" />
<figcaption aria-hidden="true">Capture d’écran du menu</figcaption>
</figure>

Sélectionner *From Template* dans la fenêtre puis le template souhaité
parmi ceux proposés pour {data.captages} :

<figure>
<img
src="https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/eau-milieux-aquatiques/data.captages/-/raw/main/inst/images/from_template.png"
alt="Capture d’écran de la fenêtre" />
<figcaption aria-hidden="true">Capture d’écran de la
fenêtre</figcaption>
</figure>

Un nouveau fichier .Rmd est créé à partir du template et peut être
enregistré par l’utilisateur sur son poste de travail.

Il faut ensuite suivre les différentes étapes (en adaptant si besoin les
paramètres) pour mettre à jour le lot de données concerné.

## Organisation de la mise à jour des lots de données

Ce logigramme présente les relations entre les lots de données et
l’ordre à respecter pour leur mise à jour :

<figure>
<img
src="https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/eau-milieux-aquatiques/data.captages/-/raw/main/inst/images/maj_data_captages_color.png"
alt="Logigramme de mise à jour des données" />
<figcaption aria-hidden="true">Logigramme de mise à jour des
données</figcaption>
</figure>

<figure>
<img
src="https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/eau-milieux-aquatiques/data.captages/-/raw/main/inst/images/maj_data_captages_legend_250.png"
alt="Légende" />
<figcaption aria-hidden="true">Légende</figcaption>
</figure>
