---
title: "Mise à jour des captages en eau potable"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development}
library(datalibaba)
library(dplyr)
library(collectr)
library(glue)
library(usethis)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# Objectif

Actualiser **la table des captages en eau potable de la région** (`captages.n_captage_eau_potable_p_r52`) à partir de la table des captages de l'ARS (`captages.n_captage_p_r52`).

# Création du dataframe des captages en eau potable

## Chargement de la table des captages

```{r load-captage, eval=FALSE}
n_captage_p_r52 <- datalibaba::importer_data(db = "si_eau",
                                             schema = "captages",
                                             table = "n_captage_p_r52")
```

## Filtre sur les captages en eau potable

Les enregistrements correspondant aux valeurs suivantes pour les usages sont sélectionnés :

- `AEP` : ADDUCTION COLLECTIVE PUBLIQUE
- `ALI` : ACTIVITE AGRO ALIMENTAIRE
- `CND` : EAU CONDITIONNEE
- `PRV` : ADDUCTION COLLECTIVE PRIVEE

Source : table `captages.n_captage_usage_direct`

```{r filter-captage-ep, eval=FALSE}
n_captage_eau_potable_p_r52 <- n_captage_p_r52 |>
  dplyr::filter(usage_captage  %in% c('AEP', 'PRV', 'ALI', 'CND'))
```

# Publication de la table en base

## Vérification de la correspondance des variables avec les champs stockés en base

```{r check-attributes, eval=FALSE}
connexion <- datalibaba::connect_to_db(db = "si_eau")
collectr::check_structure_table(connexion, 
                                df = n_captage_eau_potable_p_r52, 
                                table_name = "n_captage_eau_potable_p_r52", 
                                schema = "alimentation_eau_potable")
```

## Archivage de la version précédente de la table

```{r archive-old-table, eval=FALSE}
collectr::archive_table(database = "si_eau",
                        table = "n_captage_eau_potable_p_r52", 
                        schema = "alimentation_eau_potable",
                        new_schema = "zz_archives",
                        role = "admin")
```

## Récupération des commentaires de la version précédente de la table 

```{r get-comments-old-table, eval=FALSE}
n_captage_eau_potable_p_r52_comments <- datalibaba::get_table_comments(
  table = "n_captage_eau_potable_p_r52",
  schema = "alimentation_eau_potable",
  db = "si_eau") |> 
  dplyr::filter(!is.na(nom_col)) |> 
  dplyr::select(nom_col, commentaire) |>
  dplyr::arrange(nom_col)
```

## Publication de la table actualisée

```{r publish-new-table, eval=FALSE}
datalibaba::poster_data(data = n_captage_eau_potable_p_r52,
                        table = "n_captage_eau_potable_p_r52",
                        schema = "alimentation_eau_potable",
                        db = "si_eau",
                        pk = "code_captage",
                        overwrite = TRUE,
                        user = "admin")
```

## Publication de la description de la table actualisée

```{r publish-new-table-comment, eval=FALSE}
datalibaba::commenter_table(
  comment = glue::glue("Points de prélèvement en eau potable de la région Pays de la Loire (source : ARS, ", last_modified_date, ")"),
  table = "n_captage_eau_potable_p_r52",
  schema = "alimentation_eau_potable",
  db = "si_eau",
  user= "admin"
)
```

## Publication des commentaires des champs de la table actualisée

```{r publish-new-fields-comments, eval=FALSE}
datalibaba::post_dico_attr(dico = n_captage_p_r52_comments,
                           table = "n_captage_eau_potable_p_r52",
                           schema = "alimentation_eau_potable",
                           db = "si_eau",
                           user = "admin"
                           )
```

```{r development-create-template, eval=FALSE}
# Créer de l'arborescence et des fichiers du template
usethis::use_rmarkdown_template(
  template_name = "Mise à jour des captages en eau potable",
  template_dir = "update_captage_ep",
  template_description = "Mise à jour des captages en eau potable",
  template_create_dir = TRUE
)

# Définir les chemins source et destination
source_file <- "dev/flat_update_captage_ep.Rmd"
destination_dir <- "inst/rmarkdown/templates/update_captage_ep/skeleton"
destination_file <- file.path(destination_dir, "skeleton.Rmd")

# Copier et renommer le fichier
file.copy(from = source_file, to = destination_file, overwrite = TRUE)
message("File copied and renamed successfully.")
```

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_update_captage_ep.Rmd", vignette_name = "Mise à jour des captages en eau potable")
```

