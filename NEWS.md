# data.captages 1.3.4

- Correction du bug de sélection des stations afin de conserver celles intersectant les limites d'un SAGE ou de la région

# data.captages 1.3.3

- Amélioration de la hiérarchie des titres dans les vignettes
- Mise à jour des skeletons avec les dernières modifications

# data.captages 1.3.2

- Correction d'un problème générant des stations avec un code_bassin_versant NULL

# data.captages 1.3.1

- Ajout d'un filtre sur les stations ESU pour ne conserver que celles intersectant les SAGE de la région

# data.captages 1.3.0

- Remplacement de la source Hub'eau par une source SANDRE pour les stations ESU
- Correctif sur le paramétrage de la fonction archive_table()
- Ajout de l'user sur les fonctions de {datalibaba} écrivant en base

# data.captages 1.2.2

- Modification de l'URL du dépôt pour l'installation du package et le site `pkgdown`
- Correction d'un bug concernant l'affichage des images sur le site `pkgdown`

# data.captages 1.2.1

- Suppression de l'option `build_vignettes = TRUE` pour l'installation du package
- Modification de la syntaxe du changelog

# data.captages 1.2.0

- Ajout des templates pour faciliter l'utilisation du package
- Précisions sur l'utilisation du package dans le README

# data.captages v1.1.1

Correction d'un bug sur la fonction create_geom_hubeau() : ajout du CODE 4326 pour le paramétrage du CRS 

# data.captages v1.1.0

Ajout de la mise à jour des tables suivantes : 

- stations de mesure des qualités des nappes d'eau souterraine : `qualite_nappes_eau_souterraine.hubeau_qualite_nappes_stations`
- stations de mesures physicochimique sur des cours d'eau et plan d'eau : `qualite_cours_d_eau.hubeau_qualite_rivieres_station_pc`
- stations eau de surface (ESU) en Pays de la Loire : `station_esu`

# data.captages v1.0.0

Ajout de la mise à jour des tables suivantes :

- captages ARS en Pays de la Loire : `n_captage_p_r52`
- captages en eau potable en Pays de la Loire : `n_captage_eau_potable_p_r52`
- stations eau souterraine (ESO) en Pays de la Loire : `station_eso`
